<?php 
	//connecting to the database 
	$con = mysqli_connect("localhost","root","mysql","ar-rafi") or die('Unable to Connect...');

	//this is our upload folder 
	$upload_path = 'media/';
	
	//Getting the server ip 
	$server_ip = gethostbyname(gethostname());
	
	//creating the upload url 
	$upload_url = 'http://'.$server_ip.'/asih/'.$upload_path; 
	
	//response array 
	$response = array(); 
	
	
	if($_SERVER['REQUEST_METHOD']=='POST'){
		
		//checking the required parameters from the request 
		if(isset($_POST['id_pegawai']) and 
			isset($_POST['nama']) and
			isset($_POST['tgl_lahir']) and
			isset($_POST['alamat']) and
			isset($_POST['no_hp']) and
			isset($_POST['bagian']) and
			isset($_POST['foto']) and
			isset($_POST['username']) and 
			isset($_POST['password']) and 
			isset($_FILES['image']['name'])){
			//getting name from the request 
			$id_pegawai = $_POST['id_pegawai'];
			$nama = $_POST['nama'];
			$tgl_lahir = $_POST['tgl_lahir'];
			$alamat = $_POST['alamat'];
			$no_hp = $_POST['no_hp'];
			$bagian = $_POST['bagian'];
			$foto = $_POST['foto'];
			$username = $_POST['username'];
			$password = $_POST['password'];
			
			//getting file info from the request 
			$fileinfo = pathinfo($_FILES['image']['name']);
			
			//getting the file extension 
			$extension = $fileinfo['extension'];
			
			//file url to store in the database 
			$file_url = $upload_url . $foto . '.' . $extension;
			
			//file path to upload in the server 
			$file_path = $upload_path . $foto . '.'. $extension; 

			$full_foto = $foto . '.' . $extension;
			//trying to save the file in the directory 
			try{
				//saving the file 
				move_uploaded_file($_FILES['image']['tmp_name'],$file_path);
				$sql = "INSERT INTO `pegawai` (`id_pegawai`, `nama`, `tgl_lahir`, `alamat`, `no_hp`, `bagian`, `foto`, `username`) VALUES ('$id_pegawai', '$nama', '$tgl_lahir', '$alamat', '$no_hp', '$bagian', '$full_foto', '$username')";
				
				//adding the path and name to database 
				if(mysqli_query($con,$sql)){
					//filling response array with values 
					$hak_akses = "INSERT INTO `hak_akses` (`username`, `password`, `status`) VALUES ('$username', '$password', '$bagian')";
									
					//adding the path and name to database 
					if(mysqli_query($con,$hak_akses)){
						//filling response array with values 
						$response['error'] = false; 
					}else{
						$response['error'] = true; 
					}
				}else{
					$response['error'] = true; 
				}
			//if some error occurred 
			}catch(Exception $e){
				$response['error']=true;
				$response['message']=$e->getMessage();
			}		
			//displaying the response 
			echo json_encode($response);
			
			//closing the connection 
			mysqli_close($con);
		}else if(isset($_POST['tglPeristiwa']) and 
			isset($_POST['jamPeristiwa']) and
			isset($_POST['saksi']) and
			isset($_POST['kejadian']) and
			isset($_POST['deskripsi']) and
			isset($_POST['username']) and
			isset($_POST['foto']) and
			isset($_POST['video']) and 
			isset($_FILES['image']['name']) and 
			isset($_FILES['video']['name'])){
			//getting name from the request 
			$tglPeristiwa = $_POST['tglPeristiwa'];
			$jamPeristiwa = $_POST['jamPeristiwa'];
			$saksi = $_POST['saksi'];
			$kejadian = $_POST['kejadian'];
			$deskripsi = $_POST['deskripsi'];
			$username = $_POST['username'];
			$foto = $_POST['foto'];
			$video = $_POST['video'];
			
			//getting file info from the request 
			$fileinfo = pathinfo($_FILES['image']['name']);
			//getting the file extension 
			$extension = $fileinfo['extension'];
			//file url to store in the database 
			$file_url = $upload_url . $foto . '.' . $extension;
			//file path to upload in the server 
			$file_path = $upload_path . $foto; 
			$full_foto = $foto . '.' . $extension;

			//getting file info from the request 
			$fileinfo_video = pathinfo($_FILES['video']['name']);
			//getting the file extension 
			$extension_video = $fileinfo_video['extension'];
			//file url to store in the database 
			$file_url = $upload_url . $video . '.' . $extension_video;
			//file path to upload in the server 
			$file_path_video = $upload_path . $video; 
			$full_video = $video . '.' . $extension_video;

			//trying to save the file in the directory 
			try{
				//saving the file 
				move_uploaded_file($_FILES['image']['tmp_name'],$file_path);
				move_uploaded_file($_FILES['video']['tmp_name'],$file_path_video);
				$sql = "insert into peristiwa (tgl_peristiwa, jam_peristiwa, kejadian, video, gambar, deskripsi, username) values('$tglPeristiwa','$jamPeristiwa','$kejadian', '$video', '$foto', '$deskripsi', '$username')";
				//adding the path and name to database 
				if(mysqli_query($con,$sql)){
					$id = mysqli_insert_id($con);
					$tags = explode(',' , $saksi);
					foreach($tags as $i =>$key) {
						$i >0;
							$rr = mysqli_query($con,"insert into saksi_peristiwa (id_peristiwa, nama) values('$id','$key')");
					}
									
					//adding the path and name to database 
					if($rr){
						//filling response array with values 
						$response['error'] = false; 
					}else{
						echo "atas";
						$response['error'] = true; 
					}
				}else{
						echo "bawah";
					$response['error'] = true; 
				}
			//if some error occurred 
			}catch(Exception $e){
				$response['error']=true;
				$response['message']=$e->getMessage();
			}		
			//displaying the response 
			echo json_encode($response);
			
			//closing the connection 
			mysqli_close($con);
		}else{
			$response['error']=true;
			$response['message']='Please choose a file';
			echo json_encode($response);
		}
	}